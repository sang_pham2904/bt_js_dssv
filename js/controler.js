function layTTTuForm() {
  // lấy giá trị user nhập vào
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var mail = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = Number(document.getElementById("txtDiemToan").value);
  var ly = Number(document.getElementById("txtDiemLy").value);
  var hoa = Number(document.getElementById("txtDiemHoa").value);
  //   tạo obj chứa các giá trị đó
  // var sv = {
  //   ma: ma,
  //   ten: ten,
  //   mail: mail,
  //   matKhau: matKhau,
  //   toan: toan,
  //   ly: ly,
  //   hoa: hoa,
  // diemTB: function () {
  //   return (this.toan + this.ly + this.hoa) / 3;
  // },
  // };
  // return về cái obj đó
  var sv = new SinhVien(ma, ten, mail, matKhau, toan, ly, hoa);
  return sv;
}
function render(danhSachSV) {
  var sinhVien = "";
  for (var i = danhSachSV.length - 1; i >= 0; i--) {
    sinhVien += `<tr>
        <td>
        ${danhSachSV[i].ma}
        </td>
        <td>
        ${danhSachSV[i].ten}
        </td>
        <td>
        ${danhSachSV[i].mail}
        </td>
        <td>
        ${danhSachSV[i].tinhDiemTB()}
        </td>
        <td>
        <button class = "btn btn-danger" onclick ="xoaSV('${
          danhSachSV[i].ma
        }')">
        Xóa
        </button>
        <button class = "btn btn-primary" onclick ="suaSV('${
          danhSachSV[i].ma
        }')">
        Sửa
        </button>
        </td>
        </tr>`;
  }
  document.getElementById("tbodySinhVien").innerHTML = sinhVien;
}
// var mangThongBao = [
//   "Vui lòng nhập mã sinh viên",
//   "Vui lòng nhập tên sinh viên",
//   "Vui lòng nhập email",
//   "Vui lòng nhập mật khẩu",
//   "Vui lòng nhập điểm toán",
//   "Vui lòng nhập điểm lý",
//   "Vui lòng nhập điểm hóa",
// ];
// function kiemTraNhap(id, idthongbao, indexthongbao) {
//   var item = document.getElementById(id).value;
//   var kiemTra = true;
//   var thongBao = "";
//   if (item === "") {
//     thongBao = mangThongBao[indexthongbao];
//     document.getElementById(idthongbao).style.display = "block";
//     document.getElementById(idthongbao).innerHTML = thongBao;
//     kiemTra = false;
//   } else {
//     document.getElementById(idthongbao).style.display = "none";
//   }
//   return kiemTra;
// }

// function kiemTraNhapDB(id, idthongbao, indexthongbao) {
//   var item = document.getElementById(id).value;
//   var kiemTra = true;
//   var thongBao = "";
//   if (item === "") {
//     thongBao = mangThongBao[indexthongbao];
//     document.getElementById(idthongbao).style.display = "block";
//     document.getElementById(idthongbao).innerHTML = thongBao;
//     kiemTra = false;
//   }
//   return kiemTra;
// }

// function kiemTraKyTu(id, idthongbao) {
//   var mangKyTu = /^[A-Za-z]+$/;
//   var checkLetters = true;
//   var kiemTraLetters = document.getElementById(id).value;
//   if (kiemTraLetters.match(mangKyTu)) {
//     document.getElementById(idthongbao).style.display = "none";
//   } else {
//     document.getElementById(idthongbao).style.display = "block";
//     document.getElementById(idthongbao).innerHTML =
//       "Vui lòng chỉ nhập ký tự chữ";
//     checkLetters = false;
//   }
//   return checkLetters;
// }
// function kiemTraMail(id, idthongbao) {
//   var mangKyTu = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
//   var checkLetters = true;
//   var kiemTraLetters = document.getElementById(id).value;
//   if (kiemTraLetters.match(mangKyTu)) {
//     document.getElementById(idthongbao).style.display = "none";
//   } else {
//     document.getElementById(idthongbao).style.display = "block";
//     document.getElementById(idthongbao).innerHTML =
//       "Vui lòng nhập đúng định dạng email";
//     checkLetters = false;
//   }
//   return checkLetters;
// }
// function kiemTraTrung(danhSachSV) {
//   var sv = layTTTuForm();
//   var checkTrung = true;
//   for (var i = 0; i < danhSachSV.length; i++) {
//     if (sv.ma == danhSachSV[i].ma) {
//       document.getElementById("spanMaSV").style.display = "block";
//       document.getElementById("spanMaSV").innerHTML = "Mã sinh viên đã tồn tại";
//       checkTrung = false;
//       break;
//     } else {
//       document.getElementById("spanMaSV").style.display = "none";
//     }
//   }
//   return checkTrung;
// }
