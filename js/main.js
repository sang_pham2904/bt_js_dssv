var danhSachSV = [];
const DSSV = "DSSV";

var JSONdata = localStorage.getItem(DSSV);
if (JSONdata != null) {
  var list = JSON.parse(JSONdata);
  console.log("list: ", list);

  danhSachSV = list.map(function (item) {
    var ma = item.ma;
    var ten = item.ten;
    var mail = item.mail;
    var matKhau = item.matKhau;
    var toan = item.toan;
    var ly = item.ly;
    var hoa = item.hoa;
    var sv = new SinhVien(ma, ten, mail, matKhau, toan, ly, hoa);
    return sv;
  });
  render(danhSachSV);
}

function themSV() {
  var sv = layTTTuForm();
  // kiểm tra mã sv
  let isValid =
    kiemTraTrung(sv.ma, danhSachSV, "spanMaSV", "Mã sinh viên đã tồn tại") &&
    kiemTraDoDai(sv.ma, "spanMaSV", 4, 4) &&
    kiemTraSo(sv.ma, "spanMaSV");
  // kiểm tra mail
  isValid &= kiemTraMail(sv.mail, "spanEmailSV");
  //kiểm tra tên
  isValid &=
    kiemTraDoDai(sv.ten, "spanTenSV") && kiemTraKiTu(sv.ten, "spanTenSV");
  //Kiểm tra mật khẩu
  isValid &= kiemTraDoDai(sv.matKhau, "spanMatKhau");
  // Kiểm tra điểm
  isValid &= kiemTraNhap("txtDiemToan", "spanToan");
  isValid &= kiemTraNhap("txtDiemLy", "spanLy");
  isValid &= kiemTraNhap("txtDiemHoa", "spanHoa");
  if (isValid) {
    danhSachSV.push(sv);
    //   console.log("danhSachSV: ", danhSachSV);
    render(danhSachSV);
    var dataJSON = JSON.stringify(danhSachSV);
    localStorage.setItem(DSSV, dataJSON);
    resetForm();
  }
}

function xoaSV(id) {
  var viTri = danhSachSV.findIndex(function (item) {
    return item.ma == id;
  });
  danhSachSV.splice(viTri, 1);
  var dataJSON = JSON.stringify(danhSachSV);
  localStorage.setItem(DSSV, dataJSON);
  render(danhSachSV);
}

function suaSV(id) {
  var viTri = danhSachSV.findIndex(function (item) {
    return item.ma == id;
  });
  document.getElementById("txtMaSV").value = danhSachSV[viTri].ma;
  document.getElementById("txtTenSV").value = danhSachSV[viTri].ten;
  document.getElementById("txtEmail").value = danhSachSV[viTri].mail;
  document.getElementById("txtPass").value = danhSachSV[viTri].matKhau;
  document.getElementById("txtDiemToan").value = danhSachSV[viTri].toan;
  document.getElementById("txtDiemLy").value = danhSachSV[viTri].ly;
  document.getElementById("txtDiemHoa").value = danhSachSV[viTri].hoa;
  document.getElementById("txtMaSV").disabled = true;
}

function capNhat() {
  var sv = layTTTuForm();
  let isValid =
    kiemTraDoDai(sv.ma, "spanMaSV", 4, 4) && kiemTraSo(sv.ma, "spanMaSV");
  // kiểm tra mail
  isValid &= kiemTraMail(sv.mail, "spanEmailSV");
  //kiểm tra tên
  isValid &=
    kiemTraDoDai(sv.ten, "spanTenSV") && kiemTraKiTu(sv.ten, "spanTenSV");
  //Kiểm tra mật khẩu
  isValid &= kiemTraDoDai(sv.matKhau, "spanMatKhau");
  // Kiểm tra điểm
  isValid &= kiemTraNhap("txtDiemToan", "spanToan");
  isValid &= kiemTraNhap("txtDiemLy", "spanLy");
  isValid &= kiemTraNhap("txtDiemHoa", "spanHoa");
  if (isValid) {
    var sv = layTTTuForm();
    var viTri = danhSachSV.findIndex(function (item) {
      return item.ma == sv.ma;
    });
    danhSachSV[viTri] = sv;
    var dataJSON = JSON.stringify(danhSachSV);
    localStorage.setItem(DSSV, dataJSON);
    document.getElementById("txtMaSV").disabled = false;
    render(danhSachSV);
    resetForm();
  }
}

document.getElementById("btnSearch").addEventListener("click", function () {
  var search = document.getElementById("txtSearch").value.toLowerCase();
  // Cách 1 : search chính xác

  // var filterDSSV = danhSachSV.filter(function (item) {
  //   return item.ten.toLowerCase() == search;
  // });
  // if (filterDSSV.length > 0) {
  //   render(filterDSSV);
  // } else {
  //   alert("Không tìm thấy thông tin");
  // }

  // Cách 2 : search chỉ cần có chữ là được
  var filterDSSV = danhSachSV.filter(function (item) {
    return item.ten.toLowerCase().includes(search);
  });
  if (filterDSSV.length > 0) {
    render(filterDSSV);
  } else {
    alert("Không tìm thấy thông tin");
  }
});

function resetForm() {
  document.getElementById("formQLSV").reset();
  document.getElementById("txtMaSV").disabled = false;
}
