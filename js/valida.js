function showMessage(idthongbao, thongDiep) {
  document.getElementById(idthongbao).innerHTML = thongDiep;
}

function kiemTraTrung(id, danhSachSV, idthongbao, noiDungTB) {
  let viTri = danhSachSV.findIndex(function (item) {
    return item.ma == id;
  });
  if (viTri != -1) {
    showMessage(idthongbao, noiDungTB);
    return false;
  } else {
    showMessage(idthongbao, "");
    return true;
  }
}

function kiemTraMail(email, idthongbao) {
  const mangKyTu = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
  if (mangKyTu.test(email)) {
    showMessage(idthongbao, "");
    return true;
  } else {
    showMessage(idthongbao, "Email không hợp lệ");
    return false;
  }
}

function kiemTraDoDai(...resParam) {
  //(noidungkiemtra, idthongbao, min, max)
  if (resParam.length === 2) {
    // kiểm tra nhập
    if (resParam[0].length == 0) {
      showMessage(resParam[1], "Vui lòng điền thông tin");
      return false;
    } else {
      showMessage(resParam[1], "");
      return true;
    }
  } else if (resParam.length === 4) {
    // kiểm tra độ dài
    if (
      resParam[0].length >= resParam[2] &&
      resParam[0].length <= resParam[3]
    ) {
      showMessage(resParam[1], "");
      return true;
    } else {
      showMessage(
        resParam[1],
        resParam[2] == resParam[3]
          ? `Độ dài trường này phải là ${resParam[2]} kí tự`
          : `Độ dài phải từ ${resParam[2]} đến ${max} kí tự`
      );
      return false;
    }
  }
}

function kiemTraKiTu(noidung, idthongbao) {
  const kyTu = /^[A-Za-z]+$/;
  if (kyTu.test(noidung)) {
    showMessage(idthongbao, "");
    return true;
  } else {
    showMessage(idthongbao, "Trường này chỉ nhập kí tự chữ");
    return false;
  }
}
function kiemTraSo(noidung, idthongbao) {
  const kyTu = /^[0-9]+$/;
  if (kyTu.test(noidung)) {
    showMessage(idthongbao, "");
    return true;
  } else {
    showMessage(idthongbao, "Trường này chỉ nhập số");
    return false;
  }
}

function kiemTraNhap(id, idthongbao) {
  var item = document.getElementById(id).value;
  if (item === "") {
    showMessage(idthongbao, "Vui lòng điền thông tin");
    return false;
  } else {
    showMessage(idthongbao, "");
    return true;
  }
}
